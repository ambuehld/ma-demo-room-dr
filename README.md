# Micro Adventures  - Demo Room

## Purpose

This repository contains the reference implementation of the Micro Service Demo Room.

It intends to be a basis for other rooms that will be implemented by students that want to learn everything about Micro Services. 

## API Specification

| Method | Route | Params | Returns | Effect |
| - | - | - | - | - |
| GET | demo-room/__ping__ | no auth req. | "pong" | needed by Game Server to check if room is alive
| GET | demo-room/__description__ | no auth req. | description | delivers a short descriptive Text for the room |
| GET | demo-room/__image__ | no auth req. | room image | delivers an image of the room |
| GET | demo-room/__exits__ | no auth req. | description of all four exits | see exits json definition below |
| POST | demo-room/__start__ | no auth req.,Player, GameId | Token | Create new Visit (persistent state) on this room for Player. Returns token that contains the VisitId (Used for auth).
| GET | demo-room/__examine__ | - | RoomState | Returns current State for Room.
| GET | demo-room/objects/_object-id_/__examine__ | - | RoomState | returns more details on object and can reveal new objects
| POST | demo-room/objects/_object_id_/__interact__ | ObjectId or String | RoomState | can change the state of the object (e.g.: unlock). Can reveal new objects. Optionally can use/combine another object (e.g.: key) or a string (e.g.: password or pin)


### Player

```plantuml
@startjson
{
   "name":"John",
   "email":"john@test.ch"
}
@endjson
```

### Exits

describe the exits from outside the room

```plantuml
@startjson
{
   "north":"A sunny path leads to a light door",
   "east":"A heavy wooden door with the sign 'Demo' on it",
   "south":"A glass door that reveals the friendly demo room behind it.",
   "west":"An arrow-sign on the path reads 'Demo-Room'"

}
@endjson
```


### RoomState

for the decoration of objects we can use Bootstrap Icons.
https://icons.getbootstrap.com/

```plantuml
@startjson
{
   "objects": [
        {
            "id":"golden-sword",
            "locked":false,
            "state":"new",
            "description":"a very shiny weapon",
            "icon":"arrow-up"
        },
        {
            "id":"wooden-trunk",
            "locked":true,
            "state":"worn",
            "description":"a chest with a key-lock on it",
            "icon":"box-seam"
        },
        {
            "id":"magic-wand",
            "locked":false,
            "state":"broken",
            "description":"A wooden black wand",
            "icon":"magic"
        }
   ],
   "exits_locked":true,
   "game_uuid":"73799b4a0b7211efb1714d17d770b423",
   "message":"welcome to my room"
}
@endjson
```


## DB Structure


```plantuml

class Object {
    name : String: unique
    description : String
    active : Bool
    icon : String
}

class Player {
    name : String
    email : String
}

class RoomState {
    started_on : Datetime
    game_uuid : String
    solved : Bool
    exits_locked : Bool
}


class ObjectState {
    visible : Bool
    locked : Bool
    state : String
}

RoomState "1" o- "0..*" Player

ObjectState "0..*" o-- "1" Object 

RoomState "1" *-- "1..*" ObjectState

hide empty members
hide circle

```


## Technologies

- Flask (https://flask.palletsprojects.com/en/3.0.x/)
- ApiFlask (https://apiflask.com/)
- SQLAlchemy (https://www.sqlalchemy.org/)
- mySQL (https://www.mysql.com/de/)
- Docker Compose (https://docs.docker.com/compose/)
- pyTest (https://docs.pytest.org/en/8.0.x/)
- Gunicorn (https://gunicorn.org/)



## Installation

- clone this Repo (or a fork of it)
- change into the directory containing the file _compose.yaml_

Development Env (with Hot Reload):

```bash
docker compose up --build
```

Tests with PyTest:

```bash
docker compose -f compose.test.yaml up --build
```

Production:

```bash
docker compose -f compose.prod.yaml up --build
```

## CI/CD

Eine Konfiguration für GitLab-CI ist im Projekt angelegt. Damit die Pipeline funktioniert müssen folgende Variablen bei den Projekt- oder Gruppen-CI Variablen gesetzt werden:

- DEPLOY_TARGET - die IP-Adresse oder der DNS-Name des Ziel-Servers
- SSH_HOST_KEY - generiert durch _sudo ssh-keygen -l -f ~/.ssh/authorized_keys_ auf dem server (zum hinzufügen des Hosts zu den vertrauensvollen Servern ohne Rückfrage.)
- SSH_PRIVATE_KEY - der private SSH-Key des Servers (auf AWS EC2 normalerweise während der Erstellung generiert)
- ausserdem muss der image tag im compose.prod.yaml 




## Lizenz

© 2024. This work is openly licensed via [CC BY-NC.SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
