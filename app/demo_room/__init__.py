from apiflask import APIBlueprint

bp = APIBlueprint('demo_room', __name__)

from app.demo_room import routes


