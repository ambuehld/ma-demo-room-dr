from apiflask import APIFlask

from config import Config
from app.extensions import db

import flask_monitoringdashboard as dashboard


def create_app(config_class=Config):
    app = APIFlask(__name__)
    app.config.from_object(config_class)

    # Flask Erweiterungen initialisieren
    db.init_app(app)
        
    # Blueprints registrieren
    from app.demo_room import bp as demo_room_bp
    app.register_blueprint(demo_room_bp, url_prefix='/demo-room')

    with app.app_context():
        # setup database tables (if not already done)
        db.create_all()
        # create room objects
        from app.demo_room.routes import create_room_objects as demo_room_create_room_objects
        demo_room_create_room_objects()

    @app.route('/')
    def test_page():
        return {'message': 'micro Adventures - Demo Room'}

    if Config.MONITORING_DASHBOARD_ENABLED:
        dashboard.config.database_name=Config.MONITORING_DATABASE_URL
        dashboard.config.table_prefix=Config.MONITORING_TABLE_PREFIX
        dashboard.bind(app)

    return app