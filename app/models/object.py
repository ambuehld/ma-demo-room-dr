from apiflask import Schema
from apiflask.fields import Boolean, String, Integer
from apiflask.validators import Length

from app.extensions import db

class ObjectOut(Schema):
    id = Integer()
    name = String()
    locked = Boolean()
    state = String()
    description = String()
    icon = String()

class Object(db.Model):
    __tablename__ = 'objects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    description = db.Column(db.String(512))
    active = db.Column(db.Boolean)
    icon = db.Column(db.String(32))
