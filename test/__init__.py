import pytest
from app import create_app


# Initialize the testing environment

@pytest.fixture
def client():

    app = create_app()
    app.config.update({
        "TESTING": True,
    })

    return app.test_client()

